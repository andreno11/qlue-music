import Menu from "components/Menu";
import Navbar from "components/Navbar";

function App() {
  return (
    <main>
      <div className='main-box'>
        <Navbar />
        <Menu />
      </div>
    </main>
  );
}

export default App;