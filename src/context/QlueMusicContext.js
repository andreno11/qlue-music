import { createContext, useContext, useEffect, useState } from "react";
import filterMusic from "utils/filterMusic";

const QlueMusicContext = createContext()

const useQlueMusic = () => {
  return useContext(QlueMusicContext)
}

const QlueMusicProvider = ({children}) => {
  const [search, setSearch] = useState('')
  const [musics, setMusics] = useState([])
  const [filteredMusic, setFilteredMusic] = useState([])

  useEffect(() => {
    fetch("https://calm-lime-fawn-veil.cyclic.app/")
      .then((response) => response.json())
      .then((data) => {
        let mapping = []
        if(data){
          mapping = data.data.map((music, id) => {
            const temp = music.split(';')
            const author = temp[0]
            const name = temp[1]
            const duration = Number(temp[2])
            const artwork_url = temp[3]
            return {
              id,
              author,
              name,
              duration,
              artwork_url
            }
          })
          setMusics(mapping)
          setFilteredMusic(mapping)
        }
      });
  }, [])

  useEffect(() => {
    const timer = setTimeout(() => {
      const output = filterMusic(search, musics)
      setFilteredMusic(output)
    }, 2000)

    return () => clearTimeout(timer)
  }, [musics, search])

  return <QlueMusicContext.Provider value={{
    search,
    setSearch,
    musics,
    filteredMusic
  }}>
    {children}
  </QlueMusicContext.Provider>
}

export {useQlueMusic, QlueMusicProvider}