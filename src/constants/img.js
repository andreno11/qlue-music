import Home from 'assets/home.png'
import Profile from 'assets/avatar.png'
import Vertical from 'assets/vertical-line.png'

const Images = {
  Home,
  Profile,
  Vertical
}

export default Images