const filterMusic = (search, musics) => {
  let filter = []
  let splitText = search.split('$')
  if(search.includes('$') && splitText[0] === 'author'){
    filter = musics.filter(music => music.author.toLowerCase().includes(splitText[1]))
  }else if(search.includes('$') && splitText[0] === 'name'){
    filter = musics.filter(music => music.name.toLowerCase().includes(splitText[1]))
  } else {
    filter = musics.filter(music => music.name.toLowerCase().includes(search) || music.author.toLowerCase().includes(search))
  }

  return filter
}

export default filterMusic