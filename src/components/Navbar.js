import Images from 'constants/img'
import { useQlueMusic } from 'context/QlueMusicContext'
import React, { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router'

const Navbar = () => {
  const [currentPage, setCurrentPage] = useState('home')
  const [isOpen, setIsOpen] = useState(true)
  const {search, setSearch} = useQlueMusic()
  
  const navigate = useNavigate()
  const location = useLocation()
  
  useEffect(() => {
    if(location.pathname === '/'){
      setCurrentPage('home')
    }else{
      setCurrentPage('profile')
    }
  }, [location.pathname])

  const handleChange = (e) => {
    setSearch(e.target.value)
  }

  return (
    <div className='navbar-container'>
      <div className='navbar-header'>
        <h1>Qlue's Music</h1>
        <div className='toggle' onClick={() => setIsOpen(!isOpen)} />
      </div>
      <input className='search' placeholder='Search here...' value={search} onChange={handleChange}  />

      <div className={`show-navbar ${!isOpen && 'display-none'}`}>
        <h6 className='navbar-title'>Menu</h6>
        <div className={`list-menu cursor-pointer ${currentPage === 'profile' && 'blur'}`} onClick={() => navigate('/')}>
          <img alt='active' src={Images.Vertical} className={`active-icon ${currentPage === 'profile' && 'hidden'}`} />
          <img alt='home-icon' src={Images.Home} className='icon-menu' />
          <span>Home</span>
        </div>
        <div className={`list-menu cursor-pointer ${currentPage === 'home' && 'blur'}`} onClick={() => navigate('/profile')}>
          <img alt='active' src={Images.Vertical} className={`active-icon ${currentPage === 'home' && 'hidden'}`} />
          <img alt='profile-icon' src={Images.Profile} className='icon-menu' />
          <span>Profile</span>
        </div>
      </div>
    </div>
  )
}

export default Navbar