import React from 'react'
import Home from 'pages/Home'
import Profile from 'pages/Profile'
import { Route, Routes } from 'react-router'

const Menu = () => {
  return (
    <div className='menu-container'>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/profile' element={<Profile />} />
      </Routes>
    </div>
  )
}

export default Menu