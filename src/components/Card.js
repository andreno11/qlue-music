import React from 'react'

const Card = ({imgUrl, title, artist, duration, custClass=''}) => {
  return (
    <div className={`card ${custClass} cursor-pointer`}>
      <img className='image' alt='img-artist' src={imgUrl} />
      <p>{title}</p>
      <p>{artist}</p>
      <p>{duration}</p>
    </div>
  )
}

export default Card
