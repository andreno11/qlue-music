import Card from 'components/Card'
import React from 'react'
import { useQlueMusic } from 'context/QlueMusicContext'
import transformTime from 'utils/transformTime'

const Home = () => {
  const {musics, filteredMusic} = useQlueMusic()

  return (
    <>
      <div className='menu-header'>
        <h6 className='header-title'>Musics</h6>
        <p>{filteredMusic.length}/{musics.length}</p>
      </div>
      <div>
        {
          filteredMusic?.map(music => (
            <Card 
              key={music.id} 
              imgUrl={music?.artwork_url}
              title={music.name}
              artist={music.author}
              duration={transformTime(music.duration)}
              custClass={music.id % 2 === 0 ? 'gray':''}
              />
          ))
        } 
      </div>
    </>
  )
}

export default Home