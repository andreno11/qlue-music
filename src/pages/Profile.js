import React from 'react'

const Profile = () => {
  return (
    <>
    <div className='menu-header'>
      <h6 className='header-title'>Profile</h6>
    </div>
    <div>
      <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at nunc eleifend nisi
elementum suscipit ac posuere dolor. Aliquam a varius justo, sed venenatis erat. Sed ac eros interdum,
tempor nibh eu, tempus eros. Nam vitae ex ac massa elementum porta rhoncus quis purus. Vivamus
elementum magna non mauris elementum ultrices. Curabitur euismod in orci pellentesque gravida.
      </p>
      <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at nunc eleifend nisi
elementum suscipit ac posuere dolor. Aliquam a varius justo, sed venenatis erat. Sed ac eros interdum,
tempor nibh eu, tempus eros. Nam vitae ex ac massa elementum porta rhoncus quis purus. Vivamus
elementum magna non mauris elementum ultrices. Curabitur euismod in orci pellentesque gravida.
      </p>
      <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at nunc eleifend nisi
elementum suscipit ac posuere dolor. Aliquam a varius justo, sed venenatis erat. Sed ac eros interdum,
tempor nibh eu, tempus eros. Nam vitae ex ac massa elementum porta rhoncus quis purus. Vivamus
elementum magna non mauris elementum ultrices. Curabitur euismod in orci pellentesque gravida.
      </p>
      <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus at nunc eleifend nisi
elementum suscipit ac posuere dolor. Aliquam a varius justo, sed venenatis erat. Sed ac eros interdum,
tempor nibh eu, tempus eros. Nam vitae ex ac massa elementum porta rhoncus quis purus. Vivamus
elementum magna non mauris elementum ultrices. Curabitur euismod in orci pellentesque gravida.
      </p>
    </div>
  </>
  )
}

export default Profile